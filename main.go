package main

import (
	"fmt"
	jsonoperations "main/json"
)

func main() {
	emp := jsonoperations.EmployeeDetails{
		Name:       "Bhavika",
		EmployeeId: 110,
		Address:    "Pune",
	}

	jsonBytes, err := emp.Marshaling(emp)
	if err != nil {
		fmt.Println("\nError marshaling JSON:", err)
		return
	}
	fmt.Println("\nMarshalled data bytes:", jsonBytes)
	jsonString := string(jsonBytes)
	fmt.Println("\nMarshalled data string: ", jsonString)

	outputData, err := emp.Unmarshaling(jsonBytes)
	if err != nil {
		fmt.Println("\nError unmarshaling JSON:", err)
		return
	}
	fmt.Printf("\nUnmarshalled data string: %#v\n", outputData)
	str := fmt.Sprintf("%#v", outputData)
	fmt.Println("\nUnmarshaled data bytes: ", []byte(str))

	value, err := emp.ReadValue(jsonBytes, "address")
	if err != nil {
		fmt.Println("\nError:", err)
	} else {
		fmt.Println("\nValue:", value)
	}
}
