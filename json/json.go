package jsonoperations

import (
	"encoding/json"
	"fmt"
)

type JsonOperations interface {
	Marshaling(inputData EmployeeDetails) ([]byte, error)
	Unmarshaling(jsonBytes []byte) (EmployeeDetails, error)
	ReadValue(jsonBytes []byte, key string) (interface{}, error)
}

type EmployeeDetails struct {
	Name       string `json:"name"`
	EmployeeId int    `json:"employeeId"`
	Address    string `json:"address"`
}

// Marshaling
func (e *EmployeeDetails) Marshaling(inputData EmployeeDetails) ([]byte, error) {
	JsonBytes, err := json.MarshalIndent(inputData, "", "\t")
	if err != nil {
		return nil, err
	}
	return JsonBytes, nil
}

// Unmarshaling
func (e *EmployeeDetails) Unmarshaling(jsonBytes []byte) (EmployeeDetails, error) {
	var outputData EmployeeDetails
	err := json.Unmarshal(jsonBytes, &outputData)
	if err != nil {
		return EmployeeDetails{}, err
	}
	return outputData, nil
}

// Read particular value
func (e *EmployeeDetails) ReadValue(jsonBytes []byte, key string) (interface{}, error) {
	var inputMap map[string]interface{}
	err := json.Unmarshal(jsonBytes, &inputMap)
	if err != nil {
		return nil, err
	}
	value, ok := inputMap[key]
	if !ok {
		return nil, fmt.Errorf("key %s not found", key)
	}
	return value, nil
}
