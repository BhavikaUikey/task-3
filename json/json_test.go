package jsonoperations

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForMarshaling_Positive(t *testing.T) {
	emp := EmployeeDetails{
		Name:       "Bhavika",
		EmployeeId: 110,
		Address:    "Pune",
	}

	expected := []byte(`{
	"name": "Bhavika",
	"employeeId": 110,
	"address": "Pune"
}`)

	result, err := emp.Marshaling(emp)

	if err != nil {
		t.Errorf("Error marshaling JSON: %v", err)
	}

	assert.Equal(t, result, expected, "Test Failed")
}

func TestForMarshaling_Negative(t *testing.T) {
	emp := EmployeeDetails{
		Name:       "Bhavika",
		EmployeeId: 110,
		Address:    "Pune",
	}

	unexpected := []byte(`{
	"name": "",
	"employeeId": 110,
	"address": "Pune"
}`)

	result, err := emp.Marshaling(emp)

	if err != nil {
		t.Errorf("Error marshaling JSON: %v", err)
	}

	assert.NotEqual(t, result, unexpected, "Test Failed")
}

func TestForUnmarshaling_Positive(t *testing.T) {
	jsonBytes := []byte(`{
		"name": "Bhavika",
		"employeeId": 456,
		"address": "Pune"
	}`)

	expected := EmployeeDetails{
		Name:       "Bhavika",
		EmployeeId: 456,
		Address:    "Pune",
	}

	result, err := new(EmployeeDetails).Unmarshaling(jsonBytes)
	if err != nil {
		t.Errorf("Error unmarshaling JSON: %v", err)
	}

	assert.Equal(t, result, expected, "Test Failed")
}

func TestForUnmarshaling_Negative(t *testing.T) {
	jsonBytes := []byte(`{
		"name": "Bhavika",
		"employeeId": 456,
		"address": "Pune"
	}`)

	unexpected := EmployeeDetails{
		Name:       "",
		EmployeeId: 102,
		Address:    "Pune",
	}

	result, err := new(EmployeeDetails).Unmarshaling(jsonBytes)
	if err != nil {
		t.Errorf("Error unmarshaling JSON: %v", err)
	}

	assert.NotEqual(t, result, unexpected, "Test Failed")
}

func TestForReadValue_Positive(t *testing.T) {
	jsonBytes := []byte(`{
		"name": "Bhavika",
		"employeeId": 112,
		"address": "Pune"
	}`)

	key := "address"
	expected := "Pune"

	result, err := new(EmployeeDetails).ReadValue(jsonBytes, key)
	if err != nil {
		t.Errorf("Error reading value from JSON: %v", err)
	}

	assert.Equal(t, result, expected, "Test Failed")
}

func TestForReadValue_Negative(t *testing.T) {
	jsonBytes := []byte(`{
		"name": "Bhavika",
		"employeeId": 110,
		"address": "Pune"
	}`)

	key := "address"
	unexpected := "Mumbai"

	result, err := new(EmployeeDetails).ReadValue(jsonBytes, key)
	if err != nil {
		t.Errorf("Error reading value from JSON: %v", err)
	}

	assert.NotEqual(t, result, unexpected, "Test Failed")
}
